package pc_controller.models.abstracts.input_objects.awt_robot.input_devices.common_device_actions;

import pc_controller.models.abstracts.robot.RobotAwt;

import java.awt.*;

public abstract class CommonDeviceActions extends RobotAwt {

    public CommonDeviceActions(){
    }

    public Robot getInjectedRobot(){
        return robot;
    }

}
