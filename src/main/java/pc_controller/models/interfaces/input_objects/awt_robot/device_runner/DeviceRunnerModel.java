package pc_controller.models.interfaces.input_objects.awt_robot.device_runner;


import pc_controller.input_objects.awt_robot.input_devices.common_device_actions.action_master.ActionDeviceMaster;

public interface DeviceRunnerModel {

    void runDevice(ActionDeviceMaster commonDeviceActions);
}
