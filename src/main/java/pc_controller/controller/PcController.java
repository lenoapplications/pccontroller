package pc_controller.controller;

import pc_controller.input_objects.awt_robot.input_devices.device_master.DeviceMaster;
import pc_controller.models.abstracts.robot.RobotAwt;
import pc_controller.output_objects.screen_capturer.ScreenCapturer;

public class PcController {
    private final DeviceMaster deviceMaster;
    private final ScreenCapturer screenCapturer;

    public PcController(String location,String fileFormat,String fileNameFormat){
        RobotAwt.initRobot();
        deviceMaster = new DeviceMaster();
        screenCapturer = new ScreenCapturer();
        screenCapturer.initScreenCapturer(location,fileFormat,fileNameFormat);

    }

    public DeviceMaster getDeviceMaster() {
        return deviceMaster;
    }

    public ScreenCapturer getScreenCapturer() {
        return screenCapturer;
    }
}
