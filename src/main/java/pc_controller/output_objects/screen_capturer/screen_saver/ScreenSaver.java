package pc_controller.output_objects.screen_capturer.screen_saver;

import pc_controller.output_objects.screen_capturer.screen_shot_configuration.ScreenConfiguration;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ScreenSaver {

    public Rectangle prepeareScreen(){
        return new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
    }

    public void saveScreenShot(ScreenConfiguration screenConfiguration, BufferedImage bufferedImage){
        try {
            String saveLocation = screenConfiguration.getSaveLocationPath();
            String saveFormat = screenConfiguration.getSaveFormat();
            String fileNameFormat = screenConfiguration.getScreenShotFileName();
            ImageIO.write(bufferedImage,screenConfiguration.getSaveFormat(),createFileObject(saveLocation,saveFormat,fileNameFormat));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File createFileObject(String locationPath,String saveFormat,String screenFileNameFormat){
        int screenId = getScreenShotIdName(locationPath);
        String fullName = String.format(screenFileNameFormat,screenId,saveFormat);
        String fullAbsolutePathName = String.format(ScreenConfiguration.absolutePathFormat,locationPath,fullName);
        return new File(fullAbsolutePathName);
    }
    private int getScreenShotIdName(String locationPath){
        return new File(locationPath).listFiles().length;
    }
}
