package pc_controller.output_objects.screen_capturer.screen_shot_configuration;

public class ScreenConfiguration {
    private final String saveLocationPath;
    private final String saveFormat;
    private final String screenShotFileNameFormat;
    public final static String absolutePathFormat = "%s\\%s";

    public ScreenConfiguration(String saveLocationPath,String saveFormat,String screenShotFileName){
        this.saveLocationPath = saveLocationPath;
        this.saveFormat = saveFormat;
        this.screenShotFileNameFormat = screenShotFileName;
    }

    public String getSaveLocationPath() {
        return saveLocationPath;
    }

    public String getSaveFormat() {
        return saveFormat;
    }

    public String getScreenShotFileName() {
        return screenShotFileNameFormat;
    }
}
