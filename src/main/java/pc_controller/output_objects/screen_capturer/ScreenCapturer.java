package pc_controller.output_objects.screen_capturer;

import pc_controller.models.abstracts.robot.RobotAwt;
import pc_controller.output_objects.screen_capturer.screen_byte_converter.ScreenByteConverter;
import pc_controller.output_objects.screen_capturer.screen_saver.ScreenSaver;
import pc_controller.output_objects.screen_capturer.screen_shot_configuration.ScreenConfiguration;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ScreenCapturer extends RobotAwt {
    private ScreenSaver screenSaver;
    private ScreenByteConverter screenByteConverter;
    private ScreenConfiguration screenConfiguration;

    public void initScreenCapturer(String saveLocationPath,String saveFormat,String screenFileName){
        screenSaver = new ScreenSaver();
        screenByteConverter = new ScreenByteConverter();
        screenConfiguration = new ScreenConfiguration(saveLocationPath,saveFormat,screenFileName);
    }

    public void takeScreenShotTest(){
        Rectangle rectangle = screenSaver.prepeareScreen();
        BufferedImage bufferedImage = robot.createScreenCapture(rectangle);
        screenSaver.saveScreenShot(screenConfiguration,bufferedImage);
    }
    public byte[] getByteScreenShot(){
        Rectangle rectangle = screenSaver.prepeareScreen();
        BufferedImage bufferedImage = robot.createScreenCapture(rectangle);
        return screenByteConverter.getScreenShotBytes(screenConfiguration.getSaveFormat(),bufferedImage);
    }
}
