package pc_controller.input_objects.awt_robot.input_devices.common_device_actions.keyboard;

import pc_controller.models.abstracts.input_objects.awt_robot.input_devices.common_device_actions.CommonDeviceActions;


public class KeyboardCommonActions extends CommonDeviceActions {

    public KeyboardCommonActions(){
    }

    public void pressKey(int key){
        robot.keyPress(key);
        robot.keyRelease(key);
    }
    public void pressKey(int[] keys){
        for (int key : keys){
            robot.keyPress(key);
            robot.keyRelease(key);
        }
    }

}
