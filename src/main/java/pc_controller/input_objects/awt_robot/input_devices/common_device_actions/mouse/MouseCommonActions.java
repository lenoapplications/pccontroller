package pc_controller.input_objects.awt_robot.input_devices.common_device_actions.mouse;

import pc_controller.models.abstracts.input_objects.awt_robot.input_devices.common_device_actions.CommonDeviceActions;

import java.awt.event.InputEvent;

public class MouseCommonActions extends CommonDeviceActions {

    public void moveAndClickLeftButton(int x,int y){
        robot.mouseMove(x,y);
        click();
        robot.delay(10);
        click();
    }

    private void click(){
        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
    }

}
