package pc_controller.input_objects.awt_robot.input_devices.common_device_actions.action_master;

import pc_controller.input_objects.awt_robot.input_devices.common_device_actions.keyboard.KeyboardCommonActions;
import pc_controller.input_objects.awt_robot.input_devices.common_device_actions.mouse.MouseCommonActions;


public class ActionDeviceMaster {
    private final KeyboardCommonActions keyboardCommonActions;
    private final MouseCommonActions mouseCommonActions;

    public ActionDeviceMaster(){
        this.keyboardCommonActions = new KeyboardCommonActions();
        this.mouseCommonActions = new MouseCommonActions();
    }

    public KeyboardCommonActions getKeyboardCommonActions() {
        return keyboardCommonActions;
    }

    public MouseCommonActions getMouseCommonActions() {
        return mouseCommonActions;
    }
}
