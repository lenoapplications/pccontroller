package pc_controller.input_objects.awt_robot.input_devices.device_master;

import pc_controller.models.interfaces.input_objects.awt_robot.device_runner.DeviceRunnerModel;
import pc_controller.input_objects.awt_robot.input_devices.common_device_actions.action_master.ActionDeviceMaster;

import java.awt.Robot;

public class DeviceMaster {
    private ActionDeviceMaster actionDeviceMaster;

    public DeviceMaster(){
        actionDeviceMaster = new ActionDeviceMaster();
    }

    public void activateDevice(DeviceRunnerModel deviceRunnerModel){
        deviceRunnerModel.runDevice(actionDeviceMaster);
    }
}
