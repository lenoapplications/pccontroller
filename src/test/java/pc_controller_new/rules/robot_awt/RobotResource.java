package pc_controller_new.rules.robot_awt;

import org.junit.rules.ExternalResource;

import java.awt.*;

public class RobotResource extends ExternalResource {


    private Robot robot;

    @Override
    protected void before() throws Throwable {
        robot = new Robot();
    }

    public Robot getRobot() {
        return robot;
    }

    @Override
    protected void after() {
        robot = null;
    }
}
