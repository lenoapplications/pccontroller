package pc_controller_new.functionality_test.screen_shot_test;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import pc_controller.output_objects.screen_capturer.ScreenCapturer;
import pc_controller_new.rules.robot_awt.RobotResource;

import java.io.*;

public class ScreenShotTest {
    public static String savePath = "C:\\Users\\cacicf\\Documents\\CodeSchool\\Workspace\\gitProjects\\test_folder";
    public static String saveFormat = "png";
    public static String fileName = "screenShotTest_%s.%s";
    private ScreenCapturer screenCapturer;

    @Rule
    public RobotResource robotResource = new RobotResource();

    @Before
    public void before(){
        screenCapturer = new ScreenCapturer();
        screenCapturer.initScreenCapturer(savePath,saveFormat,fileName);
    }

    @Test
    public void checkIfScreenShotIsSaved(){
        screenCapturer.takeScreenShotTest();
    }
    @Test
    public void checkIfScreenBytesAreCorrect() throws IOException {
        byte[] bytes = screenCapturer.getByteScreenShot();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(new File(savePath+"\\TestByte.png")));
        bufferedOutputStream.write(bytes);
        bufferedOutputStream.flush();
        bufferedOutputStream.close();
    }
}
